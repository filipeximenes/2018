---
name: DjangoCon US
tier: community
site_url: https://2018.djangocon.us/
logo: djangocon.svg
---
DjangoCon US is an international community-run conference about the Django web
framework, held each year in North America. It’s six days of tutorials, talks,
and sprints in an inclusive, welcoming atmosphere.

DjangoCon US has something for everyone, from the person who develops Django
applications for a living to the person who just tinkers in their spare time.
Attendees will discover details about a range of diverse applications that
people from all over the world are building with Django, get a deeper
understanding of concepts they’re already familiar with and discover new ways
to use them, and have a lot of fun!
