---
abstract: Installing Python packages with C extensions on Linux can be a painful experience,
  but it doesn't have to be! Come learn to harness the black magic of Python wheels,
  and you too can spare your users pain... for a price.
duration: 30
level: Intermediate
presentation_url:
room: PennTop North
slot: 2018-10-06 14:40:00-04:00
speakers:
- Elana Hashman
title: Understanding The Black Magic of Python Wheels
type: talk
---

If you've ever `pip install`ed a Python package with C extensions on Linux, it was probably a painful experience, having to download and install development headers for libraries you've never even heard of. Maybe you've given up on pip and have switched to Conda. But it doesn't have to be this way! The Python Packaging Authority has been working hard to solve this problem with a new distribution format for compiled Python code, called "wheels."

In this talk, we'll descend into the practice of PEPs 513 and 571: arcane scrolls that can equip Python developers with spells to pre-compile applications and libraries in a way that allows most Linux end users to run them directly. I'll show you how to hex compiled artifacts and source code into the wheel format, harness application binary interfaces (ABIs) to use external libraries, brave the eldritch horrors of the dynamic linker, and bind these all together in the manylinux environment. Come learn to harness the black magic of Python wheels, and you too can spare your users pain... for a price.
