---
abstract: I am a former civil engineer turned software developer who will take the
  audience through how civil engineers developed their code of ethics. It has been
  a work in progress for over 3000 years! This talk will explore how software developers
  could approach engineering ethics and why we should.
duration: 30
level: All
presentation_url: https://bit.ly/2Q0wjxF
room: PennTop South
slot: 2018-10-06 13:00:00-04:00
speakers:
- Hayley Denbraver
title: 'We Are 3000 Years Behind: Let''s Talk About Engineering Ethics'
type: talk
video_url: https://youtu.be/ebDV8QOEmbE
---

Your apartment building where you wake up. 
The water you drink. 
The car you drive. 
The road on which you drive. 

In the first hour of your day, you rely on the work of several distinct branches of engineering, whose practitioners are licensed and accountable to the public. 

In the second hour of your day, you may sit down at your desk to write code that dozens, hundreds, thousands, millions of people interact with in some way--but what do they know about you, your intentions, and your training? 

A licensed Civil Engineer turned software developer will talk through how her former field approached ethics--something they have been iterating on for more than 3000 years. She will discuss how lessons learned from other engineering professions could apply to software, and where our industry may need a new approach to ethics.
