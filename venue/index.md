---
title: Venue
---

{{ site.data.event.name }} will be held at {{ site.data.event.venue.name }}.
{{ site.data.event.venue.description }}

- **{{ site.data.event.venue.name }}**
{% for line in site.data.event.venue.address %}- {{ line }}
{% endfor %}- <{{ site.data.event.venue.url }}>

{{ site.data.event.venue.map }}

{% if site.data.event.hotels.size > 0 %}
## {{ site.data.event.hotels.size | pluralize: "Hotel", "Hotels" }}

The following group rates are available:

{% for hotel in site.data.event.hotels %}
### {{ hotel.name }}

{{ hotel.rates }}

To reserve a room, visit the [booking website]({{ hotel.url }}) or call the
hotel at **{{ hotel.phone }}** and use the code **{{ hotel.code }}**.
{% if hotel.deadline %}
The room rate expires on
**{{ hotel.deadline | date: "%B %-d" }}{{ hotel.deadline | date_to_string | ordinal }}**,
so book soon.
{% endif %}
{% endfor %}
{% endif %}
